# Docker, um guia rápido para iniciantes

Esse guia possui todo o ponta-pé inicial para que um iniciante no mundo dos containers comece a sair utilizando o Docker adoidado por aí.

>  **ATENÇÃO: Para quem utiliza o Windows, recomendo a instalação e o uso do Windows Subsystem for Linux (WSL versão 2) para a utilização do docker. Este guia está focado apenas para o linux, se você está  no windows, pode utilizar essa [documentação](https://docs.microsoft.com/pt-br/windows/wsl/install-win10) para ativar o WSL 2 no seu sistema antes de dar continuidade no guia.**

## O que é Docker?

Docker é uma plataforma open source, escrita na linguagem de programação do Google, o GO, que cria e administra ambientes isolados via containers, se tornando portátil para qualquer outro sistema que possua o Docker instalado.

### O que é Container?

É um pequeno programa que compartilha o kernel do host onde você poderá isolar todo um ambiente dentro dele. Possibilitando uma utilização limitada dos recursos como HD, memória RAM e processador.

### Como funciona?

O Docker pode parecer com uma máquina virtual extremamente leve, porém não se trata disso. Diferente de uma máquina virtual, o docker não precisa carregar todo um sistema operacional para cada aplicação, ele sempre utilizará os recursos do sistema operacional do próprio host.

![https://docs.docker.com/get-started/](./images/docker-guia-rapido-01.png)

fonte: https://docs.docker.com/get-started/

### Por que utilizar?

Como o docker compartilha recursos do próprio host, resta mais espaço livre, o que deixa os processos mais rápidos e oferece uma maior disponibilidade. Todos os ambientes compartilham a mesmo modelo, que por sua vez, pode ser utilizado tanto em computadores dos desenvolvedores quanto em servidores de produção, na prática, não existe mais aquele papo de "na minha máquina funciona" pois todos os ambientes estarão exatamente iguais.

## O que é Docker Compose?

O Docker Compose é um orquestrador de containers da Docker. Porém o maestro somos nós mesmo! Através de um arquivo docker-compose.yaml podemos definir e configurar todo o ambiente necessário com apenas algumas linhas.

### Como funciona?

Parecido com o Dockerfile, o Docker Compose utiliza um arquivo de configuração, que, por padrão, é chamado de docker-compose.yaml, nele colocamos algumas linhas de configuração que quando for executado em apenas alguns instantes todo o seu ambiente definido estará pronto para uso.

### Por que utilizar?

Com o uso do docker-compose você consegue montar ambiente muito mais simples e mais rápido, principalmente quando esse ambiente exige mais de um serviço, ficando um arquivo claro de ser entendido e muito bem definido sobre toda a configuração do ambiente e de fácil edição.

## O que é um arquivo .YAML ou .YML?

 [YAML](http://yaml.org/) (acrônimo recursivo para YAML Ain’t Markup Language)

## Preparando o ambiente

Agora os passos a seguir foram tirados da documentação oficial do docker, apenas copie e cole no seu terminal os comandos que tudo dará certo.

### Como instalar o Docker?

Esse é o jeito mais rápido e pratico de instalar corretamente o docker em seu linux.

```bash
curl -sSL https://get.docker.com/ | sudo sh
```

Após a instalação, adicionar o seu usuário ao grupo docker

```bash
sudo usermod -aG docker $USER
```

Crie uma pasta .docker em seu home para configurações locais posteriores:

```bash
mkdir -p ~/.docker
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "/home/$USER/.docker" -R
```

E reiniciar o serviço

```bash
sudo service docker start
```

Para testar se o docker esta funcionando, faça logout/login e digite no terminal:

```bash
docker ps
```

Se a saída obtida for essa:

```bash
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
```

Você precisa iniciar o serviço docker:

```bash
sudo service docker start
```

esse tipo de problema acontece mais para quem esta utilizando o WSL 2, se você estiver no linux provavelmente verá a saída esperada:

```bash
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

Se você chegou até aqui, parabéns, o seu docker está instalado.

### Como instalar o Docker Compose?

Após a instalação do docker, vamos instalar o docker-compose apenas com dois comandos:

1. ```bash
   sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   ```

2. ```bash
   sudo chmod +x /usr/local/bin/docker-compose
   ```

Após o segundo comando se você digitar:

```bash
docker-compose
```

Sua saída será:

```bash
Define and run multi-container applications with Docker.

Usage:
  docker-compose [-f <arg>...] [options] [--] [COMMAND] [ARGS...]
  docker-compose -h|--help

Options:
  -f, --file FILE             Specify an alternate compose file
                              (default: docker-compose.yml)
  -p, --project-name NAME     Specify an alternate project name
                              (default: directory name)
  -c, --context NAME          Specify a context name
  --verbose                   Show more output....
```

E então seu docker-compose estará devidamente instalado.

### Como corrigir possíveis problemas de rede

Eventualmente você pode se deparar com containers com problema de comunicação, na lista de processos de containers aparentemente todos estão ok, porém quando você tenta acessa-los o container não responde, se você utiliza alguma VPN ou um PROXY pode ser que você encontre um dos problemas a seguir, se for o caso, dê uma olhada na solução abaixo:

#### Conflito de IP

Para resolver o problema de conflito de ip é necessário fazer uma alteração no arquivo /etc/docker/daemon.json colocando o novo range dos containers:

```bash
echo '{
    "dns": ["8.8.8.8","8.8.4.4"],
    "debug": true,
    "bip": "111.10.0.1/16",
    "default-address-pools": [{"base":"111.10.0.0/16","size":24},{"base":"111.20.0.0/16","size":24},{"base":"111.30.0.0/16","size":24}]
}' | sudo -E tee /etc/docker/daemon.json
```

Esse é um modelo de exemplo, provavelmente irá funcionar na sua rede, mas fique livre para alterar conforme sua necessidade. Após isso reinicie o serviço do docker:

```bash
sudo service docker restart
```

Agora o docker irá distribuir o range 111.[10|20|30].0.0/16 para os seus containers evitando assim um possível conflito de ip.

#### Rede com Proxy

Em um ambiente linux, quando você passa por um proxy, normalmente é criado as variáveis de ambiente $https_proxy, $http_proxy, $ftp_proxy etc. Verifique se elas estão criadas e devidamente configuradas para aplicar a configuração abaixo, ou caso prefira, troque as variáveis pelo número ip do seu proxy.

```bash
echo '{
"proxies": {
    "default": {
        "httpsProxy": "'$https_proxy'",
        "httpProxy": "'$http_proxy'",
        "ftpProxy": "'$ftp_proxy'",
        }
    }
}' | tee ~/.docker/config.json
```

Essa configuração abaixo é para quem tiver o problema com o registry, que mostra a seguinte mensagem: "Error response from daemon: Get https://registry-1.docker.io/v2/" caso o seu não tenha apresentado esse problema, essa configuração pode ser ignorada.

```bash
sudo -E mkdir -p /etc/systemd/system/docker.service.d

echo '[Service]
Environment="HTTP_PROXY='$http_proxy'"' | sudo -E tee /etc/systemd/system/docker.service.d/http_proxy.conf

echo '[Service]
Environment="HTTPS_PROXY='$https_proxy'"' | sudo -E tee /etc/systemd/system/docker.service.d/https_proxy.conf

sudo -E systemctl daemon-reload && 
sudo -E systemctl restart docker && 
sudo -E systemctl show --property Environment docker
```

### Rodando o primeiro container

Pronto! Depois de toda a configuração feita agora chegou a hora de testar! para saber se o seu docker está funcionando perfeitamente, rode o seguinte comando:

```` bash
docker run hello-world
````

A saída esperada será algo como:

```bash
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Already exists                                                                                            Digest: sha256:8c5aeeb6a5f3ba4883347d3747a7249f491766ca1caa47e5da5dfcf6b9b717c0
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly...
```

Se chegou até aqui, seu docker está pronto para começar a ser usado.

## Comandos mais comuns do Docker

Docker tem uma infinidade de comandos mas aqui nesse guia vou tratar dos mais comuns no dia-a-dia.

### docker pull

Serve apenas para baixar uma imagem para ser utilizada posteriormente.

**Sintaxe**:

```
docker pull <Nome da Imagem>
```

**Exemplo**:

```bash
docker pull hello-world
```

### docker run

Serve para executar uma imagem baixada porém se a imagem não existir ele vai fazer o download dela antes de executar.

**Sintaxe**:

```bash
docker run <Nome da Imagem>
```

**Exemplo**:

```bash
docker run hello-world
```

Esse comando também possui argumentos, existem alguns muito utilizados:

Para rodar um container já em background utilize o argumento '**-d**'

**Sintaxe**:

```bash
docker run -d <Nome da Imagem>
```

**Exemplo**:

```bash
docker run -d nginx
```

Para rodar um container em modo iterativo acessando o seu terminal utilize '**-it**'

**Sintaxe**:

```bash
docker run -it <Nome da Imagem> <Comando Interno do Container>
```

**Exemplo**:

```bash
docker run -it nginx /bin/bash
```

Para rodar um container em uma porta local de sua preferencia para a porta exposta nele utilize o '**-p**':

**Sintaxe**:

```bash
docker run -p <Porta Local>:<Porta do Container> <Nome da Imagem>
```

**Exemplo**:

```bash
docker run -p 8080:80 nginx
```

Para persistir dados de um container utilize o '**-v**':

**Sintaxe**:

```bash
docker run -v <Diretorio Local>:<Diretorio do Container> <Nome da Imagem>
```

**Exemplo**:

```bash
docker run -v $(pwd):/var/www/html nginx
```

Também é possível combinar todos os argumentos, por exemplo:

**Em modo iterativo:**

```bash
docker run -p 8080:80 -v $(pwd):/var/www/html -it nginx /bin/bash
```

**Em modo background:**

``` bash
docker run -p 8080:80 -v $(pwd):/var/www/html -d nginx
```

### docker ps

Por padrão mostra todos os containers que estão sendo executados.

```bash
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
99c25c4b1024        nginx               "/docker-entrypoint.…"   2 seconds ago       Up 2 seconds        0.0.0.0:8080->80/tcp   pedantic_banach
```

Nesse exemplo existe um container **nginx** com o hash **99c25c4b1024** rodando local na porta **8080**, esse hash é o **Container ID** e você utilizará muito ele quando gerenciar seus containers.

**Dica**: Quando for utilizar um **Container ID** não é necessário informar o hash completo, apenas os 4 primeiros dígitos já serve, por exemplo:

**Inspecionar esse container do exemplo:**

```
docker inspect 99c25 | grep Image
```

**Saída**:

 ```json
"Image": "sha256:f35646e83998b844c3f067e5a2cff84cdf0967627031aeda3042d78996b68d35",
"Image": "nginx",
 ```

Você também pode listar todos os containers inclusive os que foram parados utilizando o argumento '**-a**':

**Exemplo**:

```bash
docker ps -a
```

### docker start/stop

Inicia ou Para um container, para iniciar um container

### docker container

### docker rm

### docker image

### docker rmi

### docker attach

### docker info

### docker inspect

### docker logs

### docker exec

### docker build

### docker login

### docker push

## O Dockerfile

### FROM

### MAINTAINER

### ARG

### WORKDIR

### USER

### LABEL

### COPY

### ADD

### ENV

### ENTRYPOINT

### CMD

### RUN

### VOLUME

### EXPOSE

### .dockerignore

### Exemplo de um Dockerfile

## Comandos mais comuns do Docker Compose

### docker-compose up

### docker-compose down

### docker-compose ps

### docker-compose logs

### docker-compose top

### docker-compose exec

### docker-compose build

### docker-compose config

### O arquivo "docker-compose.yaml"

#### version

#### services

#### container_name

#### workdir

#### ports

#### networks

#### volumes

#### entrypoint

### Exemplo de um docker-compose.yaml

```yaml
version: '3.4'

volumes:
	db_data:
	
services:
    app:
        image: php-fpm:7.4
        container_name: app
        ports:
        - 8080:80
        workdir: /app
        volumes:
        - .:/app
        entrypoint: ["php", "-elwsS", "0.0.0.0:80"]
        depends_on:
        - db
    db:
	    image: mysql
    	container_name: db
    	environment:
      	- MYSQL_USER=root
      	- MYSQL_ALLOW_EMPTY_PASSWORD=yes
      	- MYSQL_DATABASE=database
      	volumes:
      	- db_data:/var/lib/mysql
```

## Network

O Docker possui sua própria rede, no processo de instalação do docker, ele cria automaticamente três interfaces de redes internas (bridge, host, none). Quando um novo container é iniciado, por padrão, ele utiliza a rede bridge. Para que ocorra a comunicação entre um ou mais containers é necessário que estejam na mesma rede. Para isolar a comunicação entre um ou mais containers com outros, será necessário criar uma nova rede e configura-la nos containers desejados.

### Criando uma rede docker

```bash
docker network create <NOME DA REDE>
```

### Removendo uma rede docker

```bash
docker network rm <NOME DA REDE>
```

### Conectando um container em uma rede docker

```bash
docker network connect <NOME DA REDE> <CONTAINER>
```

### Desconectando um container de uma rede docker

```bash
docker network disconnect <NOME DA REDE> <CONTAINER>
```

### Obter informações da rede docker

#### Listar todas as redes configuradas

```bash
docker network ls
```

#### Obter informação de uma rede docker

```bash
docker network inspect <NOME DA REDE>
```

## Links



## Volumes



## Registry e Repository



## Portainer.io



## Docker Cheat Sheet

### Iniciando e Parando um container

| Comando                    | Descrição                         |
| -------------------------- | --------------------------------- |
| docker start <container>   | inicia um container               |
| docker stop <container>    | finaliza um container             |
| docker restart <container> | reinicia um container             |
| docker pause <container>   | pausa um container                |
| docker unpause <container> | despausa um container             |
| docker wait <container>    | bloqueia até o container parar    |
| docker kill <container>    | envia um SIGKILL para o container |
| docker attach <container>  | conecta com um container ativo    |

### Obtendo informações do container

| Comando        | Descrição |
| -------------- | --------- |
| docker ps      |           |
| docker logs    |           |
| docker inspect |           |
| docker events  |           |
| docker port    |           |
| docker top     |           |
| docker stats   |           |
| docker diff    |           |
| docker history |           |

### Manipulando imagens

| Comando       | Descrição |
| ------------- | --------- |
| docker images |           |
| docker import |           |
| docker build  |           |
| docker commit |           |
| docker rmi    |           |
| docker load   |           |
| docker save   |           |

### Limpando a casa

| Comando                | Descrição |
| ---------------------- | --------- |
| docker system prune    |           |
| docker volume prune    |           |
| docker network prune   |           |
| docker container prune |           |
| docker image prune     |           |

## Docker Compose Cheat Sheet



## Contribuindo



## Conclusão

